package endpoints

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"nachgefragt.eventbikezero.de/internal/parse"
	"nachgefragt.eventbikezero.de/internal/util"
	"nachgefragt.eventbikezero.de/lib"
)

func questionRouter(r chi.Router) {
	r.Get("/browse", util.LogErrors("questionBrowse", questionBrowse))
	r.Route("/new", func(subRouter chi.Router) {
		subRouter.Get("/", util.LogErrors("questionNewForm", questionNewForm))
		subRouter.Post("/", util.LogErrors("questionNewPost", questionNewPost))
		subRouter.Post("/htmx/recipient", util.LogErrors("questionNewHtmxRecipient", questionNewHtmxRecipient))
	})
}

func questionBrowse(w http.ResponseWriter, r *http.Request) error {
	w.Write([]byte("Not yet"))
	return nil
}

func questionNewForm(w http.ResponseWriter, r *http.Request) error {
	templateData, err := util.RouterPreparation(r)
	if templateData["PotentialRecipients"], err = lib.GetPotentialRecipients(""); err != nil {
		return err
	}
	if err = templates.ExecuteTemplate(w, "questionNewForm", templateData); err != nil {
		return err
	}
	return nil
}

func questionNewPost(w http.ResponseWriter, r *http.Request) error {
	question, err := parse.QuestionFromPost(r)
	if err != nil {
		return err
	}
	if question.Supports[0].RemoteAddress == "" {
		question.Supports[0].RemoteAddress = r.RemoteAddr
	}
	if err = lib.AddQuestion(question); err != nil {
		return err
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
	return nil
}

func questionNewHtmxRecipient(w http.ResponseWriter, r *http.Request) error {
	templateData, err := util.RouterPreparation(r)

	recipient, err := parse.RecipientFromPost(r)
	if err != nil || recipient.ID == 0 {
		// maybe check for
		// errors.Is(result.Error, gorm.ErrRecordNotFound)
		// but other errors should also assume the Recipient is new
		templateData["RecipientNew"] = true
	}
	templateData["Recipient"] = recipient

	if err = templates.ExecuteTemplate(w, "questionNewRecipientSnippet", templateData); err != nil {
		return err
	}
	return nil
}
