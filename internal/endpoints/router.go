package endpoints

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"nachgefragt.eventbikezero.de/internal/util"
	"nachgefragt.eventbikezero.de/lib"

	"github.com/go-chi/chi/v5"
)

type templateContext map[string]interface{}

var templates *template.Template

func init() {
	// create empty template and register functions, then parse all templates
	templates = template.New("").Funcs(template.FuncMap{
		"PassData": util.PassData,
	})
	var err error
	templates, err = templates.ParseGlob("templates/*/*.go.html")
	if err != nil {
		panic("Error parsing templates: " + err.Error())
	}
}

func MainRouter() chi.Router {
	router := chi.NewRouter()
	router.Get("/", util.LogErrors("home", home))
	router.Route("/question", questionRouter)
	router.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		workDir, _ := os.Getwd()
		filesDir := http.Dir(filepath.Join(workDir, "static"))
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})
	return router
}

func home(w http.ResponseWriter, r *http.Request) error {
	templateData, err := util.RouterPreparation(r)
	templateData["LatestQuestions"] = lib.GetAllQuestions()
	if err = templates.ExecuteTemplate(w, "landingPage", templateData); err != nil {
		return err
	}
	return nil
}
