package parse

import (
	"errors"
	"net/http"

	"nachgefragt.eventbikezero.de/lib"
)

func QuestionFromPost(r *http.Request) (question lib.Question, err error) {
	recipients, err := RecipientFromPost(r)
	if err != nil {
		return
	}
	question = lib.Question{
		Question:  r.PostFormValue("question"),
		Reference: r.PostFormValue("reference"),
		Supports: []lib.Support{{
			Name:          r.PostFormValue("name"),
			RemoteAddress: r.Header.Get("X-Forwarder-For"),
		}},
		Recipients: []lib.Recipient{recipients},
	}

	return
}

func RecipientFromPost(r *http.Request) (recipient lib.Recipient, err error) {
	recipientName := r.PostFormValue("recipient")
	recipient, err = lib.SearchRecipient(recipientName)
	if err == nil {
		return
	}
	err = nil

	recipient = lib.Recipient{
		Name:        r.PostFormValue("recipient"),
		Description: r.PostFormValue("recipient_description"),
		Email:       r.PostFormValue("recipient_email"),
		EmailSource: r.PostFormValue("recipient_email_source"),
	}
	if recipient.Name == "" {
		err = errors.New("No recipient name given")
	}

	return
}
