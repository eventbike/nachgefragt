package util

import (
	"crypto/md5"
	"fmt"
	"html/template"
	"net/http"
	"os"
)

var tmpl *template.Template

func init() {
	// parse base templates and adhoc specific templates
	tmpl, _ = template.ParseGlob("templates/partials/*.go.html")
}

/* LogErrors allows to wrap around routing functions,
 * but gracefully logs errors that are returned from them.
 */
func LogErrors(route string, fn func(w http.ResponseWriter, r *http.Request) error) func(w http.ResponseWriter, r *http.Request) {
	// returns a function which matches the signature expected by the routers
	return func(w http.ResponseWriter, r *http.Request) {
		// if running the function fails ...
		if err := fn(w, r); err != nil {
			errMsg := fmt.Sprintf("%s: %s", route, err.Error())
			errHash := fmt.Sprintf("%x", md5.Sum([]byte(errMsg)))[0:6]
			fmt.Fprintf(os.Stderr, "[%s] %s\n", errHash, errMsg)

			templateData, err := RouterPreparation(r)
			templateData["errHash"] = errHash
			w.WriteHeader(500)
			if err = tmpl.ExecuteTemplate(w, "Error", templateData); err != nil {
				w.Write([]byte("Fatal Error"))
				fmt.Fprintln(os.Stderr, err.Error())
			}
		}
	}
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	templateData, err := RouterPreparation(r)
	if err = tmpl.ExecuteTemplate(w, "ErrorNotFound", templateData); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
	}
}
