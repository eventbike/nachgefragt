package util

import (
	"net/http"

	"github.com/kataras/i18n"
)

type TemplateData struct {
	Local  interface{}
	Global interface{}
}

func RouterPreparation(r *http.Request) (map[string]interface{}, error) {
	// init contexts for errors and templates
	var err error
	templateData := make(map[string]interface{})
	templateData["Header"] = struct{ Title string }{Title: "nachgefragt"}

	// auto-detect language from request and provide "tr" function to templates
	locale := i18n.GetLocale(r)
	templateData["tr"] = locale.GetMessage

	return templateData, err
}

func PassData(global, local interface{}) interface{} {
	return TemplateData{
		Global: global,
		Local:  local,
	}
}
