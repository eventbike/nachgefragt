package lib

import "time"

type Reply struct {
	ID          uint
	Question    Question
	QuestionID  uint
	Recipient   *Recipient
	RecipientID *uint
	Reply       string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
