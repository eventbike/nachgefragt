package lib

type Support struct {
	ID            uint
	Name          string
	RemoteAddress string
	Question      Question
	QuestionID    uint
}
