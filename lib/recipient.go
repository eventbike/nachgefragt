package lib

import (
	"errors"
)

type Recipient struct {
	ID          uint
	Name        string
	Description string
	Email       string
	EmailSource string
	Key         string

	Questions []*Question `gorm:"many2many:questions_recipients"`
}

func GetPotentialRecipients(query string) ([]Recipient, error) {
	recipients := []Recipient{}
	result := db.Limit(25).Find(&recipients)
	if result.Error != nil {
		return recipients, result.Error
	}
	return recipients, nil
}

func SearchRecipient(query string) (recipient Recipient, err error) {
	result := db.Where("name = ?", query).First(&recipient)
	if result.Error != nil {
		err = result.Error
		return
	}
	if recipient.ID == 0 {
		err = errors.New("No result found")
	}
	return
}
