package lib

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func OpenDB() {
	var err error
	db, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to database with error: " + err.Error())
	}
	err = db.AutoMigrate(&Question{}, &Recipient{}, &Support{}, &Reply{})
	if err != nil {
		panic("Failed to migrate with error: " + err.Error())
	}
}
