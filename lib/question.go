package lib

import (
	"math/rand"
	"time"

	"gorm.io/gorm/clause"
)

type Question struct {
	ID                     uint
	Question               string
	Reference              string
	Supports               []Support
	Recipients             []Recipient `gorm:"many2many:questions_recipients"`
	Replies                []Reply
	IsClosed               bool `gorm:"default:false;not null"`
	CreatedAt              time.Time
	QuestionSupportHelpers `gorm:"-"`
}

type QuestionSupportHelpers struct {
	SupportNames []string
	OtherCount   int
}

// TODO
func QuestionFillSupportHelp(questions []Question) []Question {
	supportsShown := 3
	for j, question := range questions {
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(question.Supports), func(i, j int) {
			question.Supports[i], question.Supports[j] = question.Supports[j], question.Supports[i]
		})
		for i, support := range question.Supports {
			if i+1 == supportsShown {
				break
			}
			questions[j].SupportNames = append(questions[j].SupportNames, support.Name)
		}
		questions[j].OtherCount = len(questions[j].Supports) - len(questions[j].SupportNames)
	}
	return questions
}

func GetAllQuestions() []Question {
	var questions []Question
	db.Preload(clause.Associations).Find(&questions)
	questions = QuestionFillSupportHelp(questions)
	return questions
}

func AddQuestion(question Question) error {
	result := db.Create(&question)
	if result.Error != nil {
		return result.Error
	}
	return nil
}
