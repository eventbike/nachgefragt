package main

import (
	"fmt"
	"net/http"

	"nachgefragt.eventbikezero.de/internal/endpoints"
	"nachgefragt.eventbikezero.de/lib"
)

func main() {
	fmt.Println("Connecting to database.")
	lib.OpenDB()
	router := endpoints.MainRouter()
	fmt.Printf("Starting to listen on localhost:4000")
	if err := http.ListenAndServe("localhost:4000", router); err != nil {
		fmt.Printf(err.Error())
	}
}
