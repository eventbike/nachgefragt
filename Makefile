.PHONY: run install fmt gofumpt dependencies
SHELL=/bin/bash

run: install fmt
	go run .
install: go.sum dependencies
go.sum:
	go get
fmt: gofumpt
	go fmt
	-gofumpt -extra -w .
gofumpt:
	go install mvdan.cc/gofumpt@latest

dependencies: static/css/pico.min.css static/js/htmx.min.js
static/css/pico.min.css:
	mkdir -p static/css
	wget https://unpkg.com/@picocss/pico@latest/css/pico.min.css -O $@
static/js/htmx.min.js:
	mkdir -p static/js
	wget https://unpkg.com/htmx.org@1.8.5 -O $@
